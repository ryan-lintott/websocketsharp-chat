using Xunit;

namespace RoomManagerTests {

    // TODO remember to test specification!

    public class RoomManagerTests {

        RoomsManager roomsManager = new RoomsManager();

        [Fact]
        public void ShouldHaveFourConnectionsAfterFourPlayersEnterRoom() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "bob", "2");
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "3");
            roomsManager.AddOrUpdatePlayer(roomId, "kate", "4");

            List<string>? connections = roomsManager.GetConnections(roomId);
            List<string> expected = new List<String> { "1", "2", "3", "4" };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ShouldHaveThreeConnectionsAfterFourPlayersEnterAndOneConnectionLeaves() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "bob", "2");
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "3");
            roomsManager.AddOrUpdatePlayer(roomId, "kate", "4");

            roomsManager.RemoveConnection("4");

            List<string>? connections = roomsManager.GetConnections(roomId);
            List<string> expected = new List<String> { "1", "2", "3", "" };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ShouldHaveOneConnectionInEachRoomAfterJoiningDifferentRooms() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId1, "niko", "1");
            roomsManager.AddOrUpdatePlayer(roomId2, "ryan", "2");

            var room1Conns = roomsManager.GetConnections(roomId1);
            var room2Conns = roomsManager.GetConnections(roomId2);
            List<string> expected1 = new List<String> { "1" };
            List<string> expected2 = new List<String> { "2" };

            Assert.True(room1Conns!.All(expected1.Contains) && room1Conns!.Count == expected1.Count);
            Assert.True(room2Conns!.All(expected2.Contains) && room2Conns!.Count == expected2.Count);
        }

        [Fact]
        public void ConnectionShouldUpdate() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "2");
            var connections = roomsManager.GetConnections(roomId);
            List<string> expected = new List<String> { "2" };

            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ConnectionShouldUpdateInOneRoomOnly() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId1, "niko", "1");
            roomsManager.AddOrUpdatePlayer(roomId2, "ryan", "2");

            roomsManager.AddOrUpdatePlayer(roomId1, "niko", "3");

            var room1Conns = roomsManager.GetConnections(roomId1);
            var room2Conns = roomsManager.GetConnections(roomId2);
            List<string> expected1 = new List<String> { "3" };
            List<string> expected2 = new List<String> { "2" };

            Assert.True(room1Conns!.All(expected1.Contains) && room1Conns!.Count == expected1.Count);
            Assert.True(room2Conns!.All(expected2.Contains) && room2Conns!.Count == expected2.Count);
        }

        [Fact]
        public void ConnectionShouldBeDeletedInOneRoomOnly() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId1, "niko", "1");
            roomsManager.AddOrUpdatePlayer(roomId2, "ryan", "2");

            roomsManager.RemoveConnection("2");

            var room1Conns = roomsManager.GetConnections(roomId1);
            var room2Conns = roomsManager.GetConnections(roomId2);
            List<string> expected1 = new List<String> { "1" };
            List<string> expected2 = new List<String> { "" };

            Assert.True(room1Conns!.All(expected1.Contains) && room1Conns!.Count == expected1.Count);
            Assert.True(room2Conns!.All(expected2.Contains) && room2Conns!.Count == expected2.Count);
        }

        [Fact]
        public void ShouldNotBeAbleToChangeTheRoomOfAConnection() {
            string roomId1 = roomsManager.CreateRoom();
            string roomId2 = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId1, "ryan", "1");

            Exception ex = Assert.Throws<Exception>(() => roomsManager.AddOrUpdatePlayer(roomId2, "ryan", "1"));
            Assert.Equal($"Player is already in a different room, (room {roomId1})", ex.Message);
        }

        [Fact]
        public void ShouldNotAllowMaxPlayersPerRoomToBeExceeded() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "bob", "2");
            roomsManager.AddOrUpdatePlayer(roomId, "kate", "3");
            roomsManager.AddOrUpdatePlayer(roomId, "niko", "4");

            Exception ex = Assert.Throws<Exception>(() => roomsManager.AddOrUpdatePlayer(roomId, "chris", "5"));
            Assert.Equal($"exceeded max player count", ex.Message);
        }

        [Fact]
        public void ShouldNotAllowMaxPlayersPerRoomToBeExceededSameConnectionId() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "bob", "2");
            roomsManager.AddOrUpdatePlayer(roomId, "kate", "3");
            roomsManager.AddOrUpdatePlayer(roomId, "niko", "4");

            Exception ex = Assert.Throws<Exception>(() => roomsManager.AddOrUpdatePlayer(roomId, "chris", "4"));
            Assert.Equal($"exceeded max player count", ex.Message);
        }

        [Fact]
        public void ShouldUpdatePlayerConnection() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "2");
            var connections = roomsManager.GetConnections(roomId);
            List<string> expected = new List<String> { "2" };

            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

        [Fact]
        public void ShouldNotAllowMaxRoomsToBeExceeded() {
            for(int i = 0; i < roomsManager.RoomLimit; i++) {
                string roomId = roomsManager.CreateRoom();
            }

            Exception ex = Assert.Throws<Exception>(() => roomsManager.CreateRoom());
            Assert.Equal($"exceeded max number of rooms", ex.Message);
        }

        [Fact]
        public void ShouldAllowPlayerToUpdateConnectionWhenRoomFull() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "bob", "2");
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "3");
            roomsManager.AddOrUpdatePlayer(roomId, "kate", "4");

            roomsManager.AddOrUpdatePlayer(roomId, "kate", "5");

            List<string>? connections = roomsManager.GetConnections(roomId);
            List<string> expected = new List<String> { "1", "2", "3", "5" };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }
        

        [Fact]
        public void ShouldAllowPlayerToUpdateConnectionWhenRoomFullSameConnectionId() {
            string roomId = roomsManager.CreateRoom();

            roomsManager.AddOrUpdatePlayer(roomId, "chris", "1");
            roomsManager.AddOrUpdatePlayer(roomId, "bob", "2");
            roomsManager.AddOrUpdatePlayer(roomId, "ryan", "3");
            roomsManager.AddOrUpdatePlayer(roomId, "kate", "4");

            roomsManager.AddOrUpdatePlayer(roomId, "kate", "4");

            List<string>? connections = roomsManager.GetConnections(roomId);
            List<string> expected = new List<String> { "1", "2", "3", "4" };
            
            Assert.True(connections!.All(expected.Contains) && connections!.Count == expected.Count);
        }

    }
}