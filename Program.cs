using Microsoft.Extensions.Logging;
using System.Net;
using System;
using System.Threading.Tasks;
using WebSocketSharp;


namespace ChatSample {
    public class Program {

        public static RoomsManager roomsManager;
        public static ILogger<WebSocketSharpChatNS.WebSocketSharpChat> chatRoomLogger;
        public static int Main(string[] args) {
            var loggerFactory = LoggerFactory.Create(builder => {
                builder.AddConsole();
            });

            var logger = loggerFactory.CreateLogger<Program>();
            chatRoomLogger = loggerFactory.CreateLogger<WebSocketSharpChatNS.WebSocketSharpChat>();
            AgonesSdkService agonesSdk = new AgonesSdkService(loggerFactory.CreateLogger<AgonesSdkService>());

            agonesSdk.StartAsync().Wait();

            roomsManager = new RoomsManager();

            for(int i = 0; i < 50; i++) {
                roomsManager.CreateRoom();
            }
            
            var server = new WebSocketSharp.Server.WebSocketServer(5000);
            server.AddWebSocketService<WebSocketSharpChatNS.WebSocketSharpChat>("/");

            server.Start();
            
            logger.LogInformation("started websocketsharp server...");
            //Console.WriteLine("started websocketsharp server...");
            Task.Delay(-1).Wait();

            //server.Stop();  
                 
            return 0;
        }
    }
}