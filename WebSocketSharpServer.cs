using WebSocketSharp.Server;
using WebSocketSharp;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Logging;


namespace WebSocketSharpChatNS {

    public class Command {
        public string type;
        public string roomId;
        public string user;
        public string message;
    }
    public class WebSocketSharpChat : WebSocketBehavior {
    
        private ILogger<WebSocketSharpChat> _logger;

        private RoomsManager _roomsManager;
    
        public WebSocketSharpChat() {
            _logger = ChatSample.Program.chatRoomLogger;
            _roomsManager = ChatSample.Program.roomsManager;
        }

        protected override void OnClose (CloseEventArgs e) {
            byte b = 0;
            _roomsManager.currentConnections.TryRemove(ID, out b);
            string? roomId = _roomsManager.RemoveConnection(ID);
            if(roomId != null) {
                var sessions = _roomsManager.GetConnections(roomId);
                if(sessions != null) {
                    // await Clients.Clients(sessions)
                    //          .SendAsync("AdminMessage", $"{Context.ConnectionId} disconnected");
                }
            }        
        }

        protected override void OnMessage (MessageEventArgs e) {
            if(!e.IsText) {
                return;
            }
            string message = e.Data;
            Command command = new Command();
            command.type = "ReceiveMessage";

            // Multicast message to all connected sessions
            var received = JsonConvert.DeserializeObject<Command>(message);
            List<string> connections = _roomsManager.GetConnections(received.roomId);

            if(received.type.Equals("Send")) {
                command.message = received.message;
                command.user = received.user;
                command.roomId = received.roomId;

                foreach(string conn in connections) {
                    try {
                        Sessions.SendTo(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(command)), conn);       
                    } catch(Exception ex) {
                        _logger.LogWarning(ex.Message);
                    }
                }

            } else if((received.type.Equals("JoinRoom"))) {
                _roomsManager.AddOrUpdatePlayer(received.roomId, 
                                                received.user, 
                                                ID);
            } else {
                // TODO: error or throw exception
            }        
        }
 
        protected override void OnOpen () {

        }

        protected override void OnError (WebSocketSharp.ErrorEventArgs e) {
             _logger.LogError($"Chat WebSocket session caught an error with code {e.Message}");
        }

    }
}